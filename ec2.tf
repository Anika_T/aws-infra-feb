resource "aws_instance" "nginx_server" {
  ami = "ami-0c7217cdde317cfec"

  instance_type = "t2.micro"
  key_name = aws_key_pair.feb_24_vpc_key.key_name 

  subnet_id     = aws_subnet.public_subnet_1.id
  associate_public_ip_address = true
  vpc_security_group_ids = [aws_security_group.allow_all.id]

  tags = {
    Name = "nginx-test-tf"
  }
}

