resource "aws_internet_gateway" "feb_24_vpc_igw" {
  vpc_id = aws_vpc.feb_24_vpc.id

  tags = {
    Name = "feb-24-vpc-igw"
  }
}

